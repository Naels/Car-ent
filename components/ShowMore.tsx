'use client'

import { showMoreProps } from "@/types"
import { useRouter } from "next/navigation"
import { CustomButton } from "."


const ShowMore = ({pageNumber, isNext} :showMoreProps) => {
  const router = useRouter()

  const handleNav = () => {
    const newLimit = (pageNumber+1) * 10
    const searchParams = new URLSearchParams(window.location.search)
    searchParams.set("limit", `${newLimit}`)

    const newPath = `${window.location.pathname}?${searchParams.toString()}`
    router.push(newPath, {scroll: false})
  }

  return (
    <div className="w-full flex-center gap-5 mt-10">
      {!isNext &&(
        <CustomButton title="Show More" btnType="button" containerStyles="bg-neutral-700 rounded-full text-white"
        handleClick={handleNav}/>
      )}
    </div>
  )
}

export default ShowMore