"use client"
import { SearchCombo } from './'

import { useRouter } from 'next/navigation'

import React, { useState } from 'react'
import Image from 'next/image'

const SearchButton = ({otherClasses }: {otherClasses:string}) => (
  <button 
    type='submit'
    className={`-ml-3 z-10 ${otherClasses}`}>
      <Image src='/magnifying-glass.svg' alt='btn' width={40} height={40} className='object-contain'/>
  </button>
)

const CustomSearch = () => {

    const [manufacturer, setManufacturer] = useState('')
    const [model, setModel ] = useState('')
    const router = useRouter()

    const handleSearch = (e: React.FormEvent<HTMLFormElement>) => {
      e.preventDefault()
      if(manufacturer === '' && model === '') return alert('Please fiil in the search informations')
      updateParamsSearch(model.toLowerCase(), manufacturer.toLowerCase())
    }

    const updateParamsSearch =(model: string, manufacturer: string) =>{
      const searchParams = new URLSearchParams(window.location.search)
      if(model) searchParams.set('model', model)
      else searchParams.delete('model')
      if(manufacturer) searchParams.set('manufacturer', manufacturer)
      else searchParams.delete('manufacturer')

      const newPath = `${window.location.pathname}?${searchParams.toString()}`
      router.push(newPath,{scroll: false})
    }

  return (
    <form action="" className="searchbar" onSubmit={handleSearch}>
        <div className="searchbar__item">
            <SearchCombo manufacturer={manufacturer} setManufacturer={setManufacturer} />
            <SearchButton otherClasses='sm:hidden'/>
        </div>
        <div className="searchbar__item">
          <Image src='/model-icon.png' width={25} height={25} alt='model icon' className='absolute w-[20px] h-[20px] ml-4'/>
          <input type='text' name='model' value={model} onChange={(e) => setModel(e.target.value)}
          placeholder='Tiguan' className='searchbar__input'/>
          <SearchButton otherClasses='sm:hidden'/>
        </div>
        <SearchButton otherClasses='mas-sm:hidden'/>

    </form>
  )
}

export default CustomSearch