import { Footer, Navbar } from '@/components'
import './globals.css'
import type { Metadata } from 'next'

export const metadata: Metadata = {
  title: 'Car-ent',
  description: 'Rent ur cars with ease',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <link rel="icon" href="/Carfav.png" />
      <body className='relative'>
        <Navbar />
        {children}
        <Footer/>
        </body>
    </html>
  )
}
