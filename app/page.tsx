import { CarCard, CustomFilter, CustomSearch, HeroBanner, ShowMore } from '@/components'
import { fuels, yearsOfProduction } from '@/constants/constants'
import { fetchCars } from '@/utils'


export default async function Home( { searchParams }) {
  const allCars = await fetchCars({
    manufacturer  : searchParams.manufacturer || '',
    model         : searchParams.model        || '',
    year          : searchParams.year         || 2022,
    fuel          : searchParams.fuel         || '',
    limit         : searchParams.limit        || 10,

  })
  const isDataEmpty = !Array.isArray(allCars) || allCars.length < 1 || !allCars
  return (
    <main className="overflow-hidden">
      <HeroBanner/>
      <div id='catalogue' className="mt-12 padding-x padding-y max-width">
        <div className="home__text-container">
          <h1 className='text-4xl font-extrabold'>Catalogue</h1>
          <p>Explore the cars you might book</p>
        </div>
        <div className="home__filters">
          <CustomSearch/>
          <div className="home__filter-container">
            <CustomFilter title="fuel" options={fuels}/>
            <CustomFilter title="year" options={yearsOfProduction}/>
          </div>
        </div>

        {!isDataEmpty ? (
          <section>
            <div className="home__cars-wrapper">
              {allCars.map((car) => (
                <CarCard car={car} />
              ))}
            </div>

            <ShowMore pageNumber={(searchParams.limit || 10)/10} isNext={(searchParams.limit || 10 ) > allCars.length} />

          </section>
        ):(
          <div className='home__error-container'>
            <h2>OOPS, no results</h2>
            <p className='text-black text-xl font-bold'>{allCars.message}</p>
          </div>
        )}

      </div>
    </main>
  )
}
